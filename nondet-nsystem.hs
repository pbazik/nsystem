import Control.Monad
import Data.List
import Text.Printf

data Nsystem = Nsystem String [(Char,String)] -- String is starting word, [(Char,String)] are rules

computeWord :: (Char,String) -> String -> String
computeWord r w =
  do
    c <- w
    if ((fst r) == c)
      then (snd r)
      else (return c)

computeGeneration :: [(Char,String)] -> [String] -> [String]
computeGeneration rs ws =
  do
    w <- ws
    r <- rs
    return (computeWord r w)

nicnajderCompute :: Nsystem -> Int -> [[String]]
nicnajderCompute (Nsystem w rs) n = take n $ iterate (computeGeneration rs) (return w)

nicnajderPrint :: Nsystem -> Int -> IO ()
nicnajderPrint s n = mapM_ (\x -> (if ((length (intercalate "\" \"" x)) <= 127)
                                   then (printf "%7d: \"%s\"\n" (length x) (intercalate "\" \"" x))
                                   else (printf "%7d: \"%s...\n" (length x) (take 126 (intercalate "\" \"" x))))) (nicnajderCompute s n)

-- examples
protoNicnajder = Nsystem "a" [('a',"ab"),('a',"c"),('b',"a")]
fancyNicnajder = Nsystem "aca" [('a',"ab"),('a',""),('b',"bab")]
retardedNicnajder = Nsystem "a" []
